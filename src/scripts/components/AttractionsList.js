export class AttractionsList {
    constructor() {
        this.list = [];
        this.selectedPicture = "";

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".touristic-points-form");
        this.pictureInput = document.querySelector(".touristic-points-form-file-input");
        this.titleInput = document.querySelector(".touristic-points-form-title-input");
        this.descriptionInput = document.querySelector(".touristic-points-form-description-input");
        this.items = document.querySelector(".touristic-points-items");
    }

    events() {
        this.form.addEventListener("submit", this.addItemToList.bind(this));

        this.pictureInput.addEventListener("change", (e) => {
            this.uploadImage(e);
        });
    }

    convertBase64(file) {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = () => {
                resolve(fileReader.result);
            };

            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    }

    uploadImage = async (event) => {
        const file = event.target.files[0];
        const base64 = await this.convertBase64(file);

        this.selectedPicture = base64;
    };

    addItemToList(event) {
        event.preventDefault();

        const itemTitle = event.target["title"].value;
        const itemDescription = event.target["description"].value;

        if (itemTitle !== "" && itemDescription !== "") {
            const item = {
                image: this.selectedPicture,
                title: itemTitle,
                description: itemDescription,
            };

            this.list.push(item);

            this.renderListItens();
            this.resetInputs();
        }
    }

    renderListItens() {
        let itemsStructure = "";

        this.list.forEach(function (item) {
            itemsStructure += `
                <li class="touristic-points-item">
                    <img src="${item.image}" class="displayImage"/>
                    <div class="touristic-points-item-wrapper">
                    <span> <h3 class="touristic-points-item-title">${item.title}<h3></span>
                    <span> <p class="touristic-points-item-description">${item.description}</p></span>
                    </div
                </li>
            `;
        });

        this.items.innerHTML = itemsStructure;
    }

    resetInputs() {
        this.titleInput.value = "";
        this.descriptionInput.value = "";
    }
}
