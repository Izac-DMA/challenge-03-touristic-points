import { AttractionsList } from "./components/AttractionsList";

document.addEventListener("DOMContentLoaded", function () {
    new AttractionsList();
});
